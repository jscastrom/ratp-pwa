Aplicación desplegada en Firebase [App Firebase](https://ratp-pwa-21e26.firebaseapp.com/)

Reportes Google LightHouse: 

*  App local: reports/127.0.0.1_2019-01-31_21-53-45.html (No PWA)
*  App Firebase: reports/ratp-pwa-21e26.firebaseapp.com_2019-01-31_21-51-06.html (Si PWA)